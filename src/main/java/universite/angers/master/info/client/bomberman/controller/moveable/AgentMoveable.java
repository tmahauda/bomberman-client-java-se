package universite.angers.master.info.client.bomberman.controller.moveable;

import java.io.Serializable;
import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.client.ClientBombermanGame;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.Agent;

/**
 * Déplacement commun à tous les agents
 *
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentMoveable implements Moveable, Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(AgentMoveable.class);

	private Player player;

	@Override
	public boolean isLegalMove(Agent agent, AgentMove move) {
		LOG.debug("Is legal move agent : " + agent);
		LOG.debug("Is legal move : " + move);
		
		if(agent == null) return false;
		if(move == null) return false;
		
		//On enregistre la demande de déplacer l'agent Bomberman
		ClientBombermanGame.getInstance().registerRequestMoveBomberman(move);
						
		//On attend de recevoir la réponse du serveur
		ClientBombermanGame.getInstance().getClient().waitFinish();
				
		//On récupère la réponse contenu dans le joueur
		this.player = ClientBombermanGame.getInstance().getPlayer();
		LOG.debug("Player move : " + this.player);
				
		return this.player.isRecord();
	}

	@Override
	public void doMove(Agent agent, AgentMove move) {
		LOG.debug("Move agent : " + agent);
		LOG.debug("Move : " + move);
		
		if(agent == null) return;
		if(move == null) return;
		
		//On attribut les nouvelles coordonnées à l'agent envoyés du serveur
		int x = this.player.getBomberman().getX();
		LOG.debug("Move x : " + x);
		
		int y = this.player.getBomberman().getY();
		LOG.debug("Move y : " + y);
		
		AgentMove m = this.player.getBomberman().getAgentMove();
		LOG.debug("Move direction : " + m);
		
		agent.setX(x);
		agent.setY(y);
		agent.setAgentMove(m);
	}
}
