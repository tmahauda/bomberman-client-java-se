package universite.angers.master.info.client.bomberman.view;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observer;
import javax.swing.JFrame;
import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.controller.Controller;
import universite.angers.master.info.client.bomberman.model.Game;

/**
 * Vue abstraite qui joue le rôle d'observateur permettant de mettre à jour en temps réel les modifications
 * faites dans le modèle Game dans les composants graphiques.
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class View extends JFrame implements Observer {
	
	private static final Logger LOG = Logger.getLogger(View.class);
	private static final long serialVersionUID = 1L;
	
	/**
	 * Controlleur qui gère les actions de l'utilisateur
	 */
	protected Controller controller;
	
	/**
	 * Modèle jeu qui contient les données à afficher dans les composants graphiques
	 */
	protected Game game;
	
	/**
	 * Constructeur de la vue
	 * @param controller
	 * @param game
	 */
	public View(Controller controller, Game game) {
		this.controller = controller;
		this.game = game;
		this.game.addObserver(this);
		
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		// Lorsqu'on quitte le plateau on se déconnecte
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
				controller.logout();
			}
		});
		
		this.createFrame();
	}
	
	public void close() {
		LOG.debug("Close");
		this.game.deleteObserver(this);
		this.setVisible(false);
		this.dispose();
	}
	
	/**
	 * Méthode qui permet de créer la vue en disposant les composants graphiques
	 * (boutons, label, etc.)
	 */
	protected abstract void createFrame();

	/**
	 * @return the controller
	 */
	public Controller getController() {
		return controller;
	}

	/**
	 * @param controller the controller to set
	 */
	public void setController(Controller controller) {
		this.controller = controller;
	}

	/**
	 * @return the game
	 */
	public Game getGame() {
		return game;
	}

	/**
	 * @param game the game to set
	 */
	public void setGame(Game game) {
		this.game = game;
	}

	@Override
	public String toString() {
		return "View [controller=" + controller + ", game=" + game + "]";
	}
}
