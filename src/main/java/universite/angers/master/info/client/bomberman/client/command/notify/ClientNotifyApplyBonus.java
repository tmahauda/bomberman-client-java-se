package universite.angers.master.info.client.bomberman.client.command.notify;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.Agent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.CapacityItem;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.InfoItem;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.ItemType;
import universite.angers.master.info.client.bomberman.model.bomberman.map.Map;
import universite.angers.master.info.network.service.Commandable;

/**
 * Classe qui permet de mettre à jour toutes les caractéristiques du bomberman à travers
 * les bonus et supprimer les bonus utilisés par le bomberman
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientNotifyApplyBonus implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ClientNotifyApplyBonus.class);

	@Override
	public boolean send(Player player) {
		LOG.debug("Notify apply bonus player : " + player);

		Agent bomberman = player.getBomberman();
		LOG.debug("Bomberman : " + bomberman);

		// On met à jour tous les items du bomberman
		for (Entry<ItemType, CapacityItem> bonusServer : bomberman.getCapacityItems().entrySet()) {
			CapacityItem bonusClient = Map.getInstance().getCurrentAgentKind().getCapacityItem(bonusServer.getKey());
			bonusClient.setCapacityActualItem(bonusServer.getValue().getCapacityActualItem());
		}

		// Suppresion des bonus
		
		// On fait une copie des bonus
		List<InfoItem> copyBonus = new ArrayList<>(Map.getInstance().getStart_Items());
		LOG.debug("Bonus copy : " + copyBonus);

		// On supprime les bonus bel et bien présent dans le server
		for (InfoItem bonusServeur : player.getItems()) {
			for (Iterator<InfoItem> it = copyBonus.iterator(); it.hasNext();) {
				InfoItem bonusClient = it.next();

				if (bonusServeur.getX() == bonusClient.getX() && bonusServeur.getY() == bonusClient.getY()) {
					it.remove();
				}
			}
		}

		// Il nous reste plus que les bonus à supprimer coté client
		for (InfoItem bonusToRemove : copyBonus) {
			for (Iterator<InfoItem> it = Map.getInstance().getStart_Items().iterator(); it.hasNext();) {
				InfoItem bonusClient = it.next();

				if (bonusToRemove.getX() == bonusClient.getX() && bonusToRemove.getY() == bonusClient.getY()) {
					it.remove();
				}
			}
		}

		return true;
	}

	@Override
	public Player receive(Object arg) {
		return null;
	}
}
