package universite.angers.master.info.client.bomberman.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import universite.angers.master.info.client.bomberman.controller.actionnable.AgentAction;
import universite.angers.master.info.client.bomberman.controller.actionnable.InfoBomb;
import universite.angers.master.info.client.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.Agent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.InfoItem;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.ennemy.AgentEnnemy;

/**
 * Classe qui permet de communiquer des infos entre client/serveur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class Player {

	/**
	 * L'adresse IP du joueur
	 */
	private String ip;
	
	/**
	 * Le port du joueur
	 */
	private String port;
	
	/**
	 * L'identifiant unique du joueur
	 */
	private String id;
	
	/**
	 * Le login pour se connecter au jeu
	 */
	private String login;
	
	/**
	 * Le mot de passe pour se connecter au jeu
	 */
	private String password;
	
	/**
	 * Les attributs permettant de transférer des infos entre client/serveur
	 */
	
	private boolean gameContinue;
	private boolean gameOver;
	private boolean record;
	private String command;
	private List<String> messages;
	private Agent bomberman;
	private AgentMove move;
	private AgentAction action;
	private String pathMap;
	private List<AgentEnnemy> agentsEnnemy;
	private List<InfoBomb> bombs;
	private List<InfoItem> items;
	private boolean wallsBrokable[][];
	private Date arrival;
	private boolean playTurn;

	public Player() {
		this.messages = new ArrayList<>();
		this.agentsEnnemy = new ArrayList<>();
		this.bombs = new ArrayList<>();
		this.items = new ArrayList<>();
	}
	
	
	/**
	 * @return the playTurn
	 */
	public boolean isPlayTurn() {
		return playTurn;
	}


	/**
	 * @param playTurn the playTurn to set
	 */
	public void setPlayTurn(boolean playTurn) {
		this.playTurn = playTurn;
	}


	/**
	 * @return the arrival
	 */
	public Date getArrival() {
		return arrival;
	}

	/**
	 * @param arrival the arrival to set
	 */
	public void setArrival(Date arrival) {
		this.arrival = arrival;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * @return the port
	 */
	public String getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(String port) {
		this.port = port;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the command
	 */
	public String getCommand() {
		return command;
	}

	/**
	 * @param command the command to set
	 */
	public void setCommand(String command) {
		this.command = command;
	}

	/**
	 * @return the messages
	 */
	public List<String> getMessages() {
		return messages;
	}

	/**
	 * @param messages the messages to set
	 */
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the record
	 */
	public boolean isRecord() {
		return record;
	}

	/**
	 * @param record the record to set
	 */
	public void setRecord(boolean record) {
		this.record = record;
	}

	/**
	 * @return the bomberman
	 */
	public Agent getBomberman() {
		return bomberman;
	}

	/**
	 * @param bomberman the bomberman to set
	 */
	public void setBomberman(Agent bomberman) {
		this.bomberman = bomberman;
	}

	/**
	 * @return the move
	 */
	public AgentMove getMove() {
		return move;
	}

	/**
	 * @param move the move to set
	 */
	public void setMove(AgentMove move) {
		this.move = move;
	}

	/**
	 * @return the action
	 */
	public AgentAction getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(AgentAction action) {
		this.action = action;
	}

	/**
	 * @return the pathMap
	 */
	public String getPathMap() {
		return pathMap;
	}

	/**
	 * @param pathMap the pathMap to set
	 */
	public void setPathMap(String pathMap) {
		this.pathMap = pathMap;
	}

	/**
	 * @return the agentsEnnemy
	 */
	public List<AgentEnnemy> getAgentsEnnemy() {
		return agentsEnnemy;
	}

	/**
	 * @param agentsEnnemy the agentsEnnemy to set
	 */
	public void setAgentsEnnemy(List<AgentEnnemy> agentsEnnemy) {
		this.agentsEnnemy = agentsEnnemy;
	}

	/**
	 * @return the bombs
	 */
	public List<InfoBomb> getBombs() {
		return bombs;
	}

	/**
	 * @param bombs the bombs to set
	 */
	public void setBombs(List<InfoBomb> bombs) {
		this.bombs = bombs;
	}

	/**
	 * @return the items
	 */
	public List<InfoItem> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(List<InfoItem> items) {
		this.items = items;
	}

	/**
	 * @return the wallsBrokable
	 */
	public boolean[][] getWallsBrokable() {
		return wallsBrokable;
	}

	/**
	 * @param wallsBrokable the wallsBrokable to set
	 */
	public void setWallsBrokable(boolean[][] wallsBrokable) {
		this.wallsBrokable = wallsBrokable;
	}
	
	/**
	 * @return the gameContinue
	 */
	public boolean isGameContinue() {
		return gameContinue;
	}

	/**
	 * @param gameContinue the gameContinue to set
	 */
	public void setGameContinue(boolean gameContinue) {
		this.gameContinue = gameContinue;
	}

	/**
	 * @return the gameOver
	 */
	public boolean isGameOver() {
		return gameOver;
	}

	/**
	 * @param gameOver the gameOver to set
	 */
	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime * result + ((agentsEnnemy == null) ? 0 : agentsEnnemy.hashCode());
		result = prime * result + ((bomberman == null) ? 0 : bomberman.hashCode());
		result = prime * result + ((bombs == null) ? 0 : bombs.hashCode());
		result = prime * result + ((command == null) ? 0 : command.hashCode());
		result = prime * result + (gameContinue ? 1231 : 1237);
		result = prime * result + (gameOver ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ip == null) ? 0 : ip.hashCode());
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((messages == null) ? 0 : messages.hashCode());
		result = prime * result + ((move == null) ? 0 : move.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((pathMap == null) ? 0 : pathMap.hashCode());
		result = prime * result + ((port == null) ? 0 : port.hashCode());
		result = prime * result + (record ? 1231 : 1237);
		result = prime * result + Arrays.deepHashCode(wallsBrokable);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (action != other.action)
			return false;
		if (agentsEnnemy == null) {
			if (other.agentsEnnemy != null)
				return false;
		} else if (!agentsEnnemy.equals(other.agentsEnnemy))
			return false;
		if (bomberman == null) {
			if (other.bomberman != null)
				return false;
		} else if (!bomberman.equals(other.bomberman))
			return false;
		if (bombs == null) {
			if (other.bombs != null)
				return false;
		} else if (!bombs.equals(other.bombs))
			return false;
		if (command == null) {
			if (other.command != null)
				return false;
		} else if (!command.equals(other.command))
			return false;
		if (gameContinue != other.gameContinue)
			return false;
		if (gameOver != other.gameOver)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ip == null) {
			if (other.ip != null)
				return false;
		} else if (!ip.equals(other.ip))
			return false;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (messages == null) {
			if (other.messages != null)
				return false;
		} else if (!messages.equals(other.messages))
			return false;
		if (move != other.move)
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (pathMap == null) {
			if (other.pathMap != null)
				return false;
		} else if (!pathMap.equals(other.pathMap))
			return false;
		if (port == null) {
			if (other.port != null)
				return false;
		} else if (!port.equals(other.port))
			return false;
		if (record != other.record)
			return false;
		if (!Arrays.deepEquals(wallsBrokable, other.wallsBrokable))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Player [ip=" + ip + ", port=" + port + ", id=" + id + ", login=" + login + ", password=" + password
				+ ", gameContinue=" + gameContinue + ", gameOver=" + gameOver + ", record=" + record + ", command="
				+ command + ", messages=" + messages + ", bomberman=" + bomberman + ", move=" + move + ", action="
				+ action + ", pathMap=" + pathMap + ", agentsEnnemy=" + agentsEnnemy + ", bombs=" + bombs + ", items="
				+ items + ", wallsBrokable=" + Arrays.toString(wallsBrokable) + "]";
	}
}