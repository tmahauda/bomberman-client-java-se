package universite.angers.master.info.client.bomberman.client.command.notify;

import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.ennemy.AgentEnnemy;
import universite.angers.master.info.client.bomberman.model.bomberman.map.Map;
import universite.angers.master.info.network.service.Commandable;

/**
 * Classe qui permet de mettre à jour les déplacements des ennemis pour la vue
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientNotifyMoveEnnemy implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ClientNotifyMoveEnnemy.class);
	
	@Override
	public boolean send(Player player) {
		LOG.debug("Notify move ennemy player : " + player);
		
		//On update les nouvelles positions des ennemis
		for(AgentEnnemy ennemyServer : player.getAgentsEnnemy()) {
			LOG.debug("Ennemy server : " + ennemyServer);
			
			for(AgentEnnemy ennemyClient : Map.getInstance().getAgentsEnnemy()) {
				LOG.debug("Ennemy client : " + ennemyClient);
				
				if(ennemyServer.getIdentifiant().equals(ennemyClient.getIdentifiant())) {
					ennemyClient.setX(ennemyServer.getX());
					ennemyClient.setY(ennemyServer.getY());
					ennemyClient.setAgentMove(ennemyServer.getAgentMove());
				}
			}
		}
		
		return true;
	}

	@Override
	public Player receive(Object arg) {
		return null;
	}
}
