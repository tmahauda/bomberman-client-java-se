package universite.angers.master.info.client.bomberman.game;

import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.client.ClientBombermanGame;
import universite.angers.master.info.client.bomberman.view.ViewLogin;

/**
 * Classe qui teste l'implémentation du jeu Bomberman en instanciant le modele BombermanGame 
 * et le controlleur ControllerBomberman
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientMainBomberman {
	
	private static final Logger LOG = Logger.getLogger(ClientMainBomberman.class);
	
	public static void main(String[] args) {
		//On affiche la vue identifiant
		ViewLogin identifiant = new ViewLogin();
		identifiant.display();
		
		//On lance un thread pour attendre que le formulaire soit complété
		Thread waitingThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				while (identifiant.getWindow().isVisible()) {
					try {
						Thread.sleep(1000);
					} catch (Exception e) {
						LOG.error(e.getMessage(), e);
					}
				}
			}
		});
		
		try {
			waitingThread.start();
			waitingThread.join();
		} 
		catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		//Si le formulaire est invalide on quitte
		LOG.debug("Valid : " + identifiant.isValidLogin());
		if(!identifiant.isValidLogin()) return;
		
		//Sinon on récupère les valeurs
		
		String login = identifiant.getLogin();
		LOG.debug("Login : " + login);
		
		String password = identifiant.getPassword();
		LOG.debug("Password : " + password);
		
		//On initialise le jeu
		ClientBombermanGame.getInstance().start();
		
		//On vérifie si le joueur peut se connecter
		ClientBombermanGame.getInstance().getControllerBombermanGame().login(login, password);
	}
}
