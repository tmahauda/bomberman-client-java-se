package universite.angers.master.info.client.bomberman.model.bomberman.agent.factory;

import universite.angers.master.info.client.bomberman.controller.actionnable.AgentAction;
import universite.angers.master.info.client.bomberman.controller.actionnable.AgentActionnablePutBomb;
import universite.angers.master.info.client.bomberman.controller.actionnable.StateBomb;
import universite.angers.master.info.client.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.client.bomberman.controller.moveable.AgentMoveable;
import universite.angers.master.info.client.bomberman.model.bomberman.ColorAgent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.Agent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.CapacityItem;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.InfoAgent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.ItemType;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.kind.AgentKindBomberman;

/**
 * Factory qui permet de créer des agents gentils
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentFactoryKind implements AgentFactory {
	
	private static AgentFactoryKind instance = null;
	
	private AgentFactoryKind() {}
	
	public static AgentFactoryKind getInstance() {
		if(instance == null)
			instance = new AgentFactoryKind();
		
		return instance;
	}
	
	@Override
	public Agent createAgent(String identifiant, String name, int x, int y, AgentMove move, char type, 
			ColorAgent color, boolean isInvincible, boolean isSick) {
		
		switch(type) {
			case AgentFactoryProvider.TYPE_AGENT_BOMBERMAN:
				AgentKindBomberman bomberman = new AgentKindBomberman(identifiant, name, x, y, move, type, color, isInvincible, isSick, StateBomb.Step1);
				
				//Les mouvements
				bomberman.setMove(new AgentMoveable());
				
				//Les actions
				bomberman.getActions().put(AgentAction.PUT_BOMB, new AgentActionnablePutBomb());
				
				//Les bonus
				this.attachBonus(bomberman);
				
				return bomberman;
				
			default:
				return null;
		}
	}	
	
	@Override
	public Agent createAgent(InfoAgent infoAgent) {

		switch(infoAgent.getType()) {
			case AgentFactoryProvider.TYPE_AGENT_BOMBERMAN:
				AgentKindBomberman bomberman = new AgentKindBomberman(infoAgent, StateBomb.Step1);
				
				//Les mouvements
				bomberman.setMove(new AgentMoveable());
				
				//Les actions
				bomberman.getActions().put(AgentAction.PUT_BOMB, new AgentActionnablePutBomb());
				
				//Les bonus
				this.attachBonus(bomberman);
				
				return bomberman;
				
			default:
				return null;
		}
	}
	
	private void attachBonus(AgentKindBomberman bomberman) {
		//Les bonus
		
		//VIE
		
		//0-->il peut mourrir
		CapacityItem itemLife = new CapacityItem(0, 10, 1, 1);
		bomberman.getCapacityItems().put(ItemType.LIFE, itemLife);
		bomberman.getCapacityItems().put(ItemType.LIFE_UP, itemLife);
		bomberman.getCapacityItems().put(ItemType.LIFE_DOWN, itemLife);
		bomberman.getCapacityItems().put(ItemType.LIFE_FULL_UP, itemLife);
		bomberman.getCapacityItems().put(ItemType.LIFE_FULL_DOWN, itemLife);
		
		//SPEED
		
		CapacityItem itemSpeed = new CapacityItem(1, 10, 1, 1);
		bomberman.getCapacityItems().put(ItemType.SPEED, itemSpeed);
		bomberman.getCapacityItems().put(ItemType.SPEED_UP, itemSpeed);
		bomberman.getCapacityItems().put(ItemType.SPEED_DOWN, itemSpeed);
		bomberman.getCapacityItems().put(ItemType.SPEED_FULL_UP, itemSpeed);
		bomberman.getCapacityItems().put(ItemType.SPEED_FULL_DOWN, itemSpeed);
		
		//BOMB
		
		CapacityItem itemBomb = new CapacityItem(1, 10, 1, 1);
		bomberman.getCapacityItems().put(ItemType.BOMB, itemBomb);
		bomberman.getCapacityItems().put(ItemType.BOMB_UP, itemBomb);
		bomberman.getCapacityItems().put(ItemType.BOMB_DOWN, itemBomb);
		bomberman.getCapacityItems().put(ItemType.BOMB_FULL_UP, itemBomb);
		bomberman.getCapacityItems().put(ItemType.BOMB_FULL_DOWN, itemBomb);
		
		//EXPLOSION
		
		CapacityItem itemFire = new CapacityItem(1, 10, 1, 1);
		bomberman.getCapacityItems().put(ItemType.FIRE, itemFire);
		bomberman.getCapacityItems().put(ItemType.FIRE_UP, itemFire);
		bomberman.getCapacityItems().put(ItemType.FIRE_DOWN, itemFire);
		bomberman.getCapacityItems().put(ItemType.FIRE_FULL_UP, itemFire);
		bomberman.getCapacityItems().put(ItemType.FIRE_FULL_DOWN, itemFire);

		//INVISIBLE
		
		CapacityItem itemInvisible = new CapacityItem(1, 10, 1, 1);
		bomberman.getCapacityItems().put(ItemType.FIRE_SUIT, itemInvisible);
		
		//MALADE
		
		CapacityItem itemSick = new CapacityItem(1, 10, 1, 1);
		bomberman.getCapacityItems().put(ItemType.SKULL, itemSick);
		
		//POINTS
		
		//Il peut avoir des points infinis
		CapacityItem point = new CapacityItem(0, Integer.MAX_VALUE, 1, 1);
		bomberman.getCapacityItems().put(ItemType.POINT, point);
	}
}
