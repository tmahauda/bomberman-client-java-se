package universite.angers.master.info.client.bomberman.client.command.notify;

import org.apache.log4j.Logger;

import universite.angers.master.info.client.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.client.bomberman.model.bomberman.map.Map;
import universite.angers.master.info.network.service.Commandable;

/**
 * Classe qui permet de mettre à jour les déplacements des ennemis pour la vue
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientNotifyMoveBomberman implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ClientNotifyMoveBomberman.class);
	
	@Override
	public boolean send(Player player) {
		LOG.debug("Notify move bomberman player : " + player);
		
		//On modifie les coordonnées x et y du bomberman
		int x = player.getBomberman().getX();
		LOG.debug("x : " + x);
		
		int y = player.getBomberman().getY();
		LOG.debug("y : " + y);
		
		AgentMove move = player.getBomberman().getAgentMove();
		LOG.debug("Move : " + move);
		
		if(Map.getInstance().getCurrentAgentKind() == null)return true;
		
		Map.getInstance().getCurrentAgentKind().setX(x);
		Map.getInstance().getCurrentAgentKind().setY(y);
		Map.getInstance().getCurrentAgentKind().setAgentMove(move);
		
		return true;
	}

	@Override
	public Player receive(Object arg) {
		return null;
	}
}
