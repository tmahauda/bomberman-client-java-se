package universite.angers.master.info.client.bomberman.view;

/**
 * Class qui permet de personnalisé les elements du ComBoBox
 * chaque element contient un nom et son chemin
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ItemComBoBox {
	
	private String chemin;
	private String name;
		
	public ItemComBoBox(String chemin, String name) {		
		this.chemin = chemin;
		this.name = name;
	}

	public String getChemin() {
		return chemin;
	}

	public void setChemin(String chemin) {
		this.chemin = chemin;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
        return name;
    }
}