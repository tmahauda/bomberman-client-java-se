package universite.angers.master.info.client.bomberman.client.command.request;

import java.util.ArrayList;
import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.controller.actionnable.AgentAction;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.network.service.Commandable;

/**
 * Classe qui permet d'envoyer une requete au serveur pour poser une bombe
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientRequestActionPutBomb implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ClientRequestActionPutBomb.class);
	
	private Player player;
	
	public ClientRequestActionPutBomb(Player player, AgentAction action) {
		LOG.debug("Request action put bomb player : " + player);
		LOG.debug("Request action put bomb action : " + action);
		
		this.player = player;
		
		this.player.setBombs(new ArrayList<>());
		this.player.setAction(action);
		this.player.setRecord(false);
		this.player.getMessages().clear();
	}
	
	@Override
	public boolean send(Player player) {
		LOG.debug("Request action put bomb player : " + player);
		
		this.player.setBombs(player.getBombs());
		this.player.setAction(player.getAction());
		this.player.setRecord(player.isRecord());
		this.player.setMessages(player.getMessages());
		
		return true;
	}

	@Override
	public Player receive(Object arg) {
		this.player.setCommand("REQUEST_ACTION_PUT_BOMB");
		return this.player;
	}
}