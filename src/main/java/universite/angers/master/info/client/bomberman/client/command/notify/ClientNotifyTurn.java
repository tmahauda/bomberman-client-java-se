package universite.angers.master.info.client.bomberman.client.command.notify;

import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.client.ClientBombermanGame;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.network.service.Commandable;

/**
 * Classe qui permet de vérifier si le jeu continue de tourner sur le serveur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientNotifyTurn implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ClientNotifyTurn.class);
	
	@Override
	public boolean send(Player player) {
		LOG.debug("Notify turn player : " + player);
		
		Commandable<Player> notify;
		
		/**
		 * Ajout de bombe
		 */
		notify = ClientBombermanGame.getInstance().getClient().getClientService().getNotifications().get("NOTIFY_ACTION_PUT_BOMB");
		notify.send(player);
		
		/**
		 * Mouvement du bomerman
		 */
		notify = ClientBombermanGame.getInstance().getClient().getClientService().getNotifications().get("NOTIFY_MOVE_BOMBERMAN");
		notify.send(player);
		
		/**
		 * Mouvements aléatoires des ennemis
		 */
		notify = ClientBombermanGame.getInstance().getClient().getClientService().getNotifications().get("NOTIFY_MOVE_ENNEMY");
		notify.send(player);
		
		/**
		 * Les bonus
		 */
		notify = ClientBombermanGame.getInstance().getClient().getClientService().getNotifications().get("NOTIFY_APPLY_BONUS");
		notify.send(player);
		
		/**
		 * Les bombes
		 */
		notify = ClientBombermanGame.getInstance().getClient().getClientService().getNotifications().get("NOTIFY_ATTACK_BOMB");
		notify.send(player);
		
		/**
		 * Attack sur le bomberman
		 */
		notify = ClientBombermanGame.getInstance().getClient().getClientService().getNotifications().get("NOTIFY_ATTACK_BOMBERMAN");
		notify.send(player);
		
		/**
		 * L'état de la partie (en cours, perdu)
		 */
		notify = ClientBombermanGame.getInstance().getClient().getClientService().getNotifications().get("NOTIFY_GAME");
		notify.send(player);
		
		return true;
	}

	@Override
	public Player receive(Object arg) {
		return null;
	}
}
