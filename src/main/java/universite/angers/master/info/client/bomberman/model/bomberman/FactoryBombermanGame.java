package universite.angers.master.info.client.bomberman.model.bomberman;

/**
 * Facotry pour construire le jeu en implémentant les règles
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class FactoryBombermanGame {

	/**
	 * Constructeur privé pour éviter l'instanciation de cette factory
	 */
	private FactoryBombermanGame() {
		
	}
	
	/**
	 * Retourne une instance de jeu pour le joueur
	 * Toutes les règles sont à enregistrés ici
	 * @param maxturn
	 * @param time
	 * @return
	 */
	public static BombermanGame getBombermanGameForPlayer(int maxturn, long time) {
		return new BombermanGame(maxturn, time);
	}
}
