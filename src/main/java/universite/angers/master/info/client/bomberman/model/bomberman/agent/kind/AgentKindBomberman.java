package universite.angers.master.info.client.bomberman.model.bomberman.agent.kind;

import universite.angers.master.info.client.bomberman.controller.actionnable.StateBomb;
import universite.angers.master.info.client.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.client.bomberman.model.bomberman.ColorAgent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.InfoAgent;

/**
 * Agent bomberman, poseur de bombe
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentKindBomberman extends AgentKind {

	private static final long serialVersionUID = 1L;

	public AgentKindBomberman(String identifiant, String name, int x, int y, AgentMove move, char type,
			ColorAgent color, boolean isInvincible, boolean isSick, StateBomb stateBomb) {
		super(identifiant, name, x, y, move, type, color, isInvincible, isSick, stateBomb);
	}

	public AgentKindBomberman(InfoAgent infoAgent, StateBomb stateBomb) {
		super(infoAgent, stateBomb);
	}
}
