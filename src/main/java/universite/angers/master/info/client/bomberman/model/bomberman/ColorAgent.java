package universite.angers.master.info.client.bomberman.model.bomberman;

/**
 * Enumération des couleurs
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public enum ColorAgent {
	BLEU,ROUGE,VERT,JAUNE,BLANC,DEFAULT
}
