package universite.angers.master.info.client.bomberman.model.bomberman.map;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import universite.angers.master.info.client.bomberman.controller.actionnable.InfoBomb;
import universite.angers.master.info.client.bomberman.controller.actionnable.StateBomb;
import universite.angers.master.info.client.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.client.bomberman.model.bomberman.ColorAgent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.Agent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.InfoAgent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.InfoItem;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.ennemy.AgentEnnemy;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.kind.AgentKind;

/** 
 * Classe qui permet de charger une carte de Bomberman à partir d'un fichier de layout d'extension .lay
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class Map implements Serializable {

	private static final long serialVersionUID = 1L;
	protected static Map instance = null;
	
	protected String filename;
	protected String niveau;
	protected int size_x;
	protected int size_y;
	protected boolean walls[][];
	protected boolean start_brokable_walls[][];
	protected ArrayList<InfoAgent> start_Agents;
	protected ArrayList<InfoItem> start_Items;
	protected ArrayList<InfoBomb> start_Bombs;
	protected ArrayList<Agent> agents;
	protected ArrayList<AgentKind> agentsKind;
	protected ArrayList<AgentEnnemy> agentsEnnemy;
	protected AgentKind currentAgentKind;

	/**
	 * Constructeur protected pour l'utiliser dans la recherche en IA
	 */
	protected Map() {
		this.start_Agents = new ArrayList<>();
		this.start_Items = new ArrayList<>();
		this.start_Bombs = new ArrayList<>();
		this.agents = new ArrayList<>();
		this.agentsKind = new ArrayList<>();
		this.agentsEnnemy = new ArrayList<>();
	}
	
	public static synchronized Map getInstance() {
		if(instance == null)
			instance = new Map();
		
		return instance;
	}	
	
	/**
	 * Charger une map à partir d'un fichier .lay
	 * @throws Exception
	 */
	public void load() throws Exception {
		//On instancie les listes
		this.start_Agents = new ArrayList<>();
		this.start_Items = new ArrayList<>();
		this.start_Bombs = new ArrayList<>();
		this.agents = new ArrayList<>();
		this.agentsKind = new ArrayList<>();
		this.agentsEnnemy = new ArrayList<>();

		InputStream flux = null;
		InputStreamReader lecture = null;
		BufferedReader tampon = null;
		
		try {
			
			flux = this.getClass().getResourceAsStream(this.filename);
			lecture = new InputStreamReader(flux);
			tampon = new BufferedReader(lecture);
			
			String ligne;

			int nbX=0;
			int nbY=0;

			while ((ligne = tampon.readLine())!=null) {
				ligne = ligne.trim();
				if (nbX==0) {nbX = ligne.length();}
				else if (nbX != ligne.length()) {
					flux.close();
					lecture.close();
					tampon.close();
					throw new Exception("Toutes les lignes doivent avoir la même longueur");
				}
				nbY++;
			}			
			flux.close();
			lecture.close();
			tampon.close();
				
			size_x = nbX;
			size_y = nbY;
			
			walls = new boolean [size_x][size_y];
			start_brokable_walls  = new boolean [size_x][size_y];
				
			flux = this.getClass().getResourceAsStream(this.filename);
			lecture = new InputStreamReader(flux);
			tampon = new BufferedReader(lecture);
			int y=0;
		
			ColorAgent[] color = ColorAgent.values();
			int cpt_col = 0;
			
			int id = 0;
			while ((ligne=tampon.readLine())!=null)
			{
				ligne=ligne.trim();

				for(int x=0;x<ligne.length();x++)
				{
					
					if (ligne.charAt(x)=='%') 
						walls[x][y]=true; 
						
					else walls[x][y]=false;
					
					if (ligne.charAt(x)=='$') 
						start_brokable_walls[x][y]=true; 
					else start_brokable_walls[x][y]=false;
					
					if (ligne.charAt(x)=='E' || ligne.charAt(x)=='V' || ligne.charAt(x)=='R') {
						start_Agents.add(new InfoAgent(""+id,""+id,x,y,AgentMove.STOP,ligne.charAt(x),ColorAgent.DEFAULT,false,false,StateBomb.Step1));	
					}
					
					if (ligne.charAt(x)=='B') {
						ColorAgent col;
						if (cpt_col < color.length) col = color[cpt_col];
						else col = ColorAgent.DEFAULT;	
						start_Agents.add(new InfoAgent(""+id,""+id,x,y,AgentMove.STOP, ligne.charAt(x),col,false,false,StateBomb.Step1));
						cpt_col++;
					}
						
				}
				y++;
				id++;
			}
			flux.close();
			lecture.close();
			tampon.close();
			
			//On verifie que le labyrinthe est clos			
			for(int x=0;x<size_x;x++) if (!walls[x][0]) throw new Exception("Mauvais format du fichier: la carte doit etre close");
			for(int x=0;x<size_x;x++) if (!walls[x][size_y-1]) throw new Exception("Mauvais format du fichier: la carte doit etre close");
			for(y=0;y<size_y;y++) if (!walls[0][y]) throw new Exception("Mauvais format du fichier: la carte doit etre close");
			for(y=0;y<size_y;y++) if (!walls[size_x-1][y]) throw new Exception("Mauvais format du fichier: la carte doit etre close");
			
			} catch (Exception e) {
				System.out.println("Erreur : "+e.getMessage());
			} finally {
				if(flux != null) flux.close();
				if(lecture != null) lecture.close();
				if(tampon != null) tampon.close();
			}
	}

	/**
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @param filename the filename to set
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * @return the size_x
	 */
	public int getSize_x() {
		return size_x;
	}

	/**
	 * @param size_x the size_x to set
	 */
	public void setSize_x(int size_x) {
		this.size_x = size_x;
	}

	/**
	 * @return the size_y
	 */
	public int getSize_y() {
		return size_y;
	}

	/**
	 * @param size_y the size_y to set
	 */
	public void setSize_y(int size_y) {
		this.size_y = size_y;
	}

	/**
	 * @return the walls
	 */
	public boolean[][] getWalls() {
		return walls;
	}

	/**
	 * @param walls the walls to set
	 */
	public void setWalls(boolean[][] walls) {
		this.walls = walls;
	}

	/**
	 * @return the start_brokable_walls
	 */
	public boolean[][] getStart_brokable_walls() {
		return start_brokable_walls;
	}

	/**
	 * @param start_brokable_walls the start_brokable_walls to set
	 */
	public void setStart_brokable_walls(boolean[][] start_brokable_walls) {
		this.start_brokable_walls = start_brokable_walls;
	}

	/**
	 * @return the start_Agents
	 */
	public ArrayList<InfoAgent> getStart_Agents() {
		return start_Agents;
	}

	/**
	 * @param start_Agents the start_Agents to set
	 */
	public void setStart_Agents(ArrayList<InfoAgent> start_Agents) {
		this.start_Agents = start_Agents;
	}

	/**
	 * @return the start_Items
	 */
	public ArrayList<InfoItem> getStart_Items() {
		return start_Items;
	}

	/**
	 * @param start_Items the start_Items to set
	 */
	public void setStart_Items(ArrayList<InfoItem> start_Items) {
		this.start_Items = start_Items;
	}

	/**
	 * @return the start_Bombs
	 */
	public ArrayList<InfoBomb> getStart_Bombs() {
		return start_Bombs;
	}

	/**
	 * @param start_Bombs the start_Bombs to set
	 */
	public void setStart_Bombs(ArrayList<InfoBomb> start_Bombs) {
		this.start_Bombs = start_Bombs;
	}

	/**
	 * @return the agents
	 */
	public ArrayList<Agent> getAgents() {
		return agents;
	}

	/**
	 * @param agents the agents to set
	 */
	public void setAgents(ArrayList<Agent> agents) {
		this.agents = agents;
	}

	/**
	 * @return the agentsKind
	 */
	public ArrayList<AgentKind> getAgentsKind() {
		return agentsKind;
	}

	/**
	 * @param agentsKind the agentsKind to set
	 */
	public void setAgentsKind(ArrayList<AgentKind> agentsKind) {
		this.agentsKind = agentsKind;
	}

	/**
	 * @return the agentsEnnemy
	 */
	public ArrayList<AgentEnnemy> getAgentsEnnemy() {
		return agentsEnnemy;
	}

	/**
	 * @param agentsEnnemy the agentsEnnemy to set
	 */
	public void setAgentsEnnemy(ArrayList<AgentEnnemy> agentsEnnemy) {
		this.agentsEnnemy = agentsEnnemy;
	}

	/**
	 * @return the currentAgentKind
	 */
	public AgentKind getCurrentAgentKind() {
		return currentAgentKind;
	}

	/**
	 * @param currentAgentKind the currentAgentKind to set
	 */
	public void setCurrentAgentKind(AgentKind currentAgentKind) {
		this.currentAgentKind = currentAgentKind;
	}

	/**
	 * @return the niveau
	 */
	public String getNiveau() {
		return niveau;
	}

	/**
	 * @param niveau the niveau to set
	 */
	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agents == null) ? 0 : agents.hashCode());
		result = prime * result + ((agentsEnnemy == null) ? 0 : agentsEnnemy.hashCode());
		result = prime * result + ((agentsKind == null) ? 0 : agentsKind.hashCode());
		result = prime * result + ((currentAgentKind == null) ? 0 : currentAgentKind.hashCode());
		result = prime * result + ((filename == null) ? 0 : filename.hashCode());
		result = prime * result + size_x;
		result = prime * result + size_y;
		result = prime * result + ((start_Agents == null) ? 0 : start_Agents.hashCode());
		result = prime * result + ((start_Bombs == null) ? 0 : start_Bombs.hashCode());
		result = prime * result + ((start_Items == null) ? 0 : start_Items.hashCode());
		result = prime * result + Arrays.deepHashCode(start_brokable_walls);
		result = prime * result + Arrays.deepHashCode(walls);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Map other = (Map) obj;
		if (agents == null) {
			if (other.agents != null)
				return false;
		} else if (!agents.equals(other.agents))
			return false;
		if (agentsEnnemy == null) {
			if (other.agentsEnnemy != null)
				return false;
		} else if (!agentsEnnemy.equals(other.agentsEnnemy))
			return false;
		if (agentsKind == null) {
			if (other.agentsKind != null)
				return false;
		} else if (!agentsKind.equals(other.agentsKind))
			return false;
		if (currentAgentKind == null) {
			if (other.currentAgentKind != null)
				return false;
		} else if (!currentAgentKind.equals(other.currentAgentKind))
			return false;
		if (filename == null) {
			if (other.filename != null)
				return false;
		} else if (!filename.equals(other.filename))
			return false;
		if (size_x != other.size_x)
			return false;
		if (size_y != other.size_y)
			return false;
		if (start_Agents == null) {
			if (other.start_Agents != null)
				return false;
		} else if (!start_Agents.equals(other.start_Agents))
			return false;
		if (start_Bombs == null) {
			if (other.start_Bombs != null)
				return false;
		} else if (!start_Bombs.equals(other.start_Bombs))
			return false;
		if (start_Items == null) {
			if (other.start_Items != null)
				return false;
		} else if (!start_Items.equals(other.start_Items))
			return false;
		if (!Arrays.deepEquals(start_brokable_walls, other.start_brokable_walls))
			return false;
		if (!Arrays.deepEquals(walls, other.walls))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Map [filename=" + filename + ", size_x=" + size_x + ", size_y=" + size_y
				+ ", start_Agents=" + start_Agents + ", start_Items=" + start_Items + ", start_Bombs=" + start_Bombs
				+ ", agents=" + agents + ", agentsKind=" + agentsKind + ", agentsEnnemy=" + agentsEnnemy
				+ ", currentAgentKind=" + currentAgentKind + "]";
	}
}