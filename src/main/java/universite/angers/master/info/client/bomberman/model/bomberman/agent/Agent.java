package universite.angers.master.info.client.bomberman.model.bomberman.agent;

import java.io.Serializable;
import java.util.HashMap;
import universite.angers.master.info.client.bomberman.controller.actionnable.Actionnable;
import universite.angers.master.info.client.bomberman.controller.actionnable.AgentAction;
import universite.angers.master.info.client.bomberman.controller.actionnable.StateBomb;
import universite.angers.master.info.client.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.client.bomberman.controller.moveable.Moveable;
import universite.angers.master.info.client.bomberman.model.bomberman.ColorAgent;

/**
 * Agent du jeu
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class Agent extends InfoAgent implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Mouvement possible de l'agent (marcher, voler, sauter, poursuivre, ...
	 */
	protected transient Moveable move;

	/**
	 * Toutes les actions que peut réaliser les agents
	 */
	protected transient java.util.Map<AgentAction, Actionnable> actions;

	/**
	 * Liste des capacités des agents - Vie du bomberman - Portée de l'explosion des
	 * bombes - Nombre de bombes qu'il peut poser simultanément - Vitesse du
	 * bomberman - Attaque des ennemis sur le bomberman - ...
	 */
	protected java.util.Map<ItemType, CapacityItem> capacityItems;

	public Agent() {
		super();
	}

	public Agent(String identifiant, String name, int x, int y, AgentMove agentMove, char type, ColorAgent color,
			boolean isInvincible, boolean isSick) {
		super(identifiant, name, x, y, agentMove, type, color, isInvincible, isSick, StateBomb.Step1);
		this.actions = new HashMap<>();
		this.capacityItems = new HashMap<>();
	}

	public Agent(InfoAgent infoAgent) {
		this(infoAgent.getIdentifiant(), infoAgent.getName(), infoAgent.getX(), infoAgent.getY(),
				infoAgent.getAgentMove(), infoAgent.getType(), infoAgent.getColor(), infoAgent.isInvincible(),
				infoAgent.isSick());
	}

	/**
	 * Vérifier si un mouvement est réalisable vers une direction
	 * 
	 * @param move vers le haut, bas, gauche ou droite
	 * @return vrai ou faux
	 */
	public boolean isLegalMove(AgentMove move) {
		return this.move.isLegalMove(this, move);
	}

	/**
	 * Réaliser un mouvement vers une direction
	 * 
	 * @param move vers le haut, bas, gauche ou droite
	 */
	public void doMove(AgentMove move) {
		if (this.isLegalMove(move))
			this.move.doMove(this, move);
	}

	/**
	 * @return the move
	 */
	public Moveable getMove() {
		return move;
	}

	/**
	 * @param move the move to set
	 */
	public void setMove(Moveable move) {
		this.move = move;
	}

	/**
	 * Vérifier si l'action peut être réalisé
	 * 
	 * @param agentAction
	 * @return
	 */
	public boolean isLegalAction(AgentAction agentAction) {
		if (agentAction == null)
			return false;
		if (!this.actions.containsKey(agentAction))
			return false;

		Actionnable action = this.actions.get(agentAction);
		if (action == null)
			return false;

		return action.isLegalAction(this, agentAction);
	}

	/**
	 * L'action à réaliser
	 * 
	 * @param agentAction
	 */
	public void doAction(AgentAction agentAction) {
		if (agentAction == null)
			return;
		if (!this.actions.containsKey(agentAction))
			return;

		Actionnable action = this.actions.get(agentAction);
		if (action == null)
			return;

		if (action.isLegalAction(this, agentAction)) {
			action.doAction(this, agentAction);
		}
	}

	/**
	 * @return the actions
	 */
	public java.util.Map<AgentAction, Actionnable> getActions() {
		return actions;
	}

	/**
	 * @param actions the actions to set
	 */
	public void setActions(java.util.Map<AgentAction, Actionnable> actions) {
		this.actions = actions;
	}

	/**
	 * @return the capacityItems
	 */
	public java.util.Map<ItemType, CapacityItem> getCapacityItems() {
		return capacityItems;
	}

	/**
	 * @param capacityItems the capacityItems to set
	 */
	public void setCapacityItems(java.util.Map<ItemType, CapacityItem> capacityItems) {
		this.capacityItems = capacityItems;
	}

	/**
	 * @return the capacity
	 */
	public CapacityItem getCapacityItem(ItemType itemType) {
		if (this.capacityItems.containsKey(itemType))
			return this.capacityItems.get(itemType);
		else
			return null;
	}
}