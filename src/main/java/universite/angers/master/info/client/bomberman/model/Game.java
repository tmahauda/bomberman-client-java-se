package universite.angers.master.info.client.bomberman.model;

import java.io.Serializable;
import java.util.Observable;
import org.apache.log4j.Logger;

/**
 * Classe abstraite qui hérite Observable et implémente Runnable pour :
 * - Jouer le rôle de sujet afin de notifier les observateurs (les vues) qu'elle a changée d'état à l'aide du pattern Observateur ;
 * - Créer une architecture générale d’un jeu séquentiel à l’aide du pattern Patron de méthode ;
 * - Garder la main sur l’interface graphique pendant que le jeu s’éxécute en tâche de fond, comme le contrôle du temps de la simulation du jeu,
 *   ainsi que de lancer un grand nombre de simulations de jeu en parallèle pour évaluer différentes stratégies de comportement des agents.
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class Game extends Observable implements Runnable, Serializable {

	private static final Logger LOG = Logger.getLogger(Game.class);
	private static final long serialVersionUID = 1L;
	
	/**
     * Compter le nombre de tour courant
     */
	protected int turn;
	
	/**
     * Le nombre de tour maximum
     */
	protected int maxturn;
	
	/**
     * Savoir si le jeu est lancé
     * Jeu lancé = true
     * Jeu en pause = false
     */
	protected boolean isRunning;
	
	/**
     * Thread du jeu
     */
	protected Thread thread;
	
	/**
     * Corresponds au temps de pause entre chaque tour en millisecondes.
     */
	protected long time;
	
	/**
	 * La partie est-elle perdu ?
	 * 
	 * La partie est terminé. Soit parce que
	 * L'agent bomberman est mort
	 * Le nombre de tour est hors limite
	 */
	protected boolean gameOver;
	
	/**
	 * La partie est-elle toujours en cours ?
	 * 
	 * Le jeu continue tant qu'il y a des agents sur le plateau
	 * et que l'agent bomberman n'est pas mort
	 */
	protected boolean gameContinue;
	
	/**
     * Constructeur de Game
     * @param maxturn le nombre de tour maximum
     * @param time le temps d'execution entre chaque tour
     */
	public Game(int maxturn, long time) {		
		this.maxturn = maxturn;
		LOG.debug("Max turn : " + maxturn);
		
		this.time = time;
		LOG.debug("Time : " + time);
		
		this.gameOver = false;
		this.gameContinue = true;
	}
	
	/**
     * Méthode qui permet d'intialiser le jeu
     */
	public abstract void initializeGame();
	
	/**
	 * Méthode qui initialise le jeu en remettant le compteur du nombre de tours turn à zéro, 
	 * met isRunning à la valeur false et appelle la méthode abstraite void initializeGame(), 
	 * non implémentée pour l’instant. Elle sera implémentée par les classes concrètes qui héritent de Game.
	 */
	public void init() {
		// au debut y pas de tours
		this.turn = 0;
		
		// au debut le jeu n'est pas en route
		this.isRunning = false;

		this.initializeGame();
	}
	
	/**
	 * Méthode qui incrémente le compteur de tour du jeu et effectue
	 * un seul tour du jeu en appelant la méthode abstraite void takeTurn() si le jeu n’est pas
	 * terminé. Si le jeu est terminé, elle met le booléen isRunning à la valeur false
	 * puis fait appel à la méthode abstraite void gameOver() et qui permettra d’afficher un
	 * message de fin du jeu. Pour savoir si le jeu est terminé, elle appel la méthode abstraite
	 * boolean gameContinue() et vérifie si le nombre maximum de tours est atteint.
	 */
	public void step() {
		
		LOG.debug("Game continue : " + this.gameContinue);
		
		// Si le jeu n'est pas terminé
		if(this.gameContinue) {
			this.turn++;				
			this.setChanged();
			this.notifyObservers(false);
		}
		// Si le jeu est terminé
		else {
			this.setChanged();
			this.notifyObservers(true);
		}
	}
	
	/**
	 * Méthode qui lance le jeu en pas à pas avec la méthode step() jusqu’à
	 * la fin tant que le jeu n’est pas mis en pause (testé par un flag booléen isRunning).
	 */
	@Override
	public void run() {
		while(this.isRunning) {
			//Pause pour réguler l'execution du jeu entre chaque tour
			try {
				Thread.sleep(this.time);
				
				//Puis execute le tour
				this.step();
			} 
			catch (Exception e) {
				LOG.error(e.getMessage());
			}
		}		
	}
	
	/**
	 * Arreter le jeu
	 */
	public void close() {
		this.isRunning = false;		
		this.deleteObservers();
		this.thread.interrupt();
	}
	
	/**
	 * Méthode concrète stop() qui met en pause le jeu en mettant le flag booléen isRunning à false.
	 */
	public void stop() {
		this.isRunning = false;
	}
	
	/**
	 * Méthode qui met l’attribut isRunning à la valeur true puis instancie l’attribut thread 
	 * avec un nouvel objet Thread qui contient le jeu courant (thread = new Thread(this);) 
	 * et enfin fait appel à la méthode start() de cet objet thread pour lancer le jeu. 
	 * Remarque : l’appel à cette méthode lancera automatiquement la méthode run().
	 */
	public void launch() {
		this.isRunning = true;
		this.thread = new Thread(this);
		this.thread.start();
	}

	/**
	 * @return the turn
	 */
	public int getTurn() {
		return turn;
	}

	/**
	 * @param turn the turn to set
	 */
	public void setTurn(int turn) {
		this.turn = turn;
	}

	/**
	 * @return the maxturn
	 */
	public int getMaxturn() {
		return maxturn;
	}

	/**
	 * @param maxturn the maxturn to set
	 */
	public void setMaxturn(int maxturn) {
		this.maxturn = maxturn;
	}

	/**
	 * @return the isRunning
	 */
	public boolean isRunning() {
		return isRunning;
	}

	/**
	 * @param isRunning the isRunning to set
	 */
	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}

	/**
	 * @return the time
	 */
	public long getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(long time) {
		this.time = time;
	}

	/**
	 * @return the thread
	 */
	public Thread getThread() {
		return thread;
	}

	/**
	 * @param thread the thread to set
	 */
	public void setThread(Thread thread) {
		this.thread = thread;
	}

	/**
	 * @return the gameOver
	 */
	public boolean isGameOver() {
		return gameOver;
	}

	/**
	 * @param gameOver the gameOver to set
	 */
	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}

	/**
	 * @return the gameContinue
	 */
	public boolean isGameContinue() {
		return gameContinue;
	}

	/**
	 * @param gameContinue the gameContinue to set
	 */
	public void setGameContinue(boolean gameContinue) {
		this.gameContinue = gameContinue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (gameContinue ? 1231 : 1237);
		result = prime * result + (gameOver ? 1231 : 1237);
		result = prime * result + (isRunning ? 1231 : 1237);
		result = prime * result + maxturn;
		result = prime * result + ((thread == null) ? 0 : thread.hashCode());
		result = prime * result + (int) (time ^ (time >>> 32));
		result = prime * result + turn;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Game other = (Game) obj;
		if (gameContinue != other.gameContinue)
			return false;
		if (gameOver != other.gameOver)
			return false;
		if (isRunning != other.isRunning)
			return false;
		if (maxturn != other.maxturn)
			return false;
		if (thread == null) {
			if (other.thread != null)
				return false;
		} else if (!thread.equals(other.thread))
			return false;
		if (time != other.time)
			return false;
		if (turn != other.turn)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Game [turn=" + turn + ", maxturn=" + maxturn + ", isRunning=" + isRunning + ", thread=" + thread
				+ ", time=" + time + ", gameOver=" + gameOver + ", gameContinue=" + gameContinue + "]";
	}
}