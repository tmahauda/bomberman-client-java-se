package universite.angers.master.info.client.bomberman.client.command.notify;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.controller.actionnable.InfoBomb;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.Agent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.CapacityItem;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.InfoItem;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.ItemType;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.ennemy.AgentEnnemy;
import universite.angers.master.info.client.bomberman.model.bomberman.map.Map;
import universite.angers.master.info.network.service.Commandable;

/**
 * Classe qui permet de mettre à jour l'états des bombes
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientNotifyAttackBomb implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ClientNotifyAttackBomb.class);

	@Override
	public boolean send(Player player) {
		LOG.debug("Notify attack bomb player : " + player);

		if(player == null) return true;
		
		/**
		 * Mise a jour de l'etat des bombes 
		 * verification par position (X,Y) de la bomb bombServeur/bombClient
		 */
		if(player.getBombs() == null) return true;
		for (InfoBomb bombServeur : player.getBombs()) {
			for (InfoBomb bombClient : Map.getInstance().getStart_Bombs()) {
				if (bombServeur.getX() == bombClient.getX() && bombServeur.getY() == bombClient.getY()) {
					bombClient.setStateBomb(bombServeur.getStateBomb());
				}
			}
		}
		
		/**
		 * Mise a jour des bombe présentes
		 * verification coordonnées x et y car les bombes ne bougent pas
		 */
		
		//On fait une copie des bombes
		List<InfoBomb> copyBombs = new ArrayList<>(Map.getInstance().getStart_Bombs());
		LOG.debug("Bombs copy : " + copyBombs);
		
		//On supprime les bombes bel et bien présent dans le server
		for (InfoBomb bombServeur : player.getBombs()) {
			for (Iterator<InfoBomb> it = copyBombs.iterator(); it.hasNext(); ) {
				InfoBomb bombClient = it.next();
				
				if (bombServeur.getX() == bombClient.getX() && bombServeur.getY() == bombClient.getY()) {
					it.remove();
				}
			}
		}
		
		//Il nous reste plus que les bombes à supprimer coté client
		for (InfoBomb bombToRemove : copyBombs) {
			for (Iterator<InfoBomb> it = Map.getInstance().getStart_Bombs().iterator(); it.hasNext(); ) {
				InfoBomb bombClient = it.next();
				
				if (bombToRemove.getX() == bombClient.getX() && bombToRemove.getY() == bombClient.getY()) {
					it.remove();
				}
			}
		}
		
		/**
		 * Mise a jour des ennemis présents
		 * verification par identifiant d'Agent car les ennemis bougent, impossible de vérifier par coordonnées
		 */
		
		//On fait une copie des ennemis
		List<AgentEnnemy> copyEnnemies = new ArrayList<>(Map.getInstance().getAgentsEnnemy());
		LOG.debug("Ennemies copy : " + copyEnnemies);
		
		//On supprime les ennemis bel et bien présent dans le server
		for (AgentEnnemy ennemyServer : player.getAgentsEnnemy()) {
			for (Iterator<AgentEnnemy> it = copyEnnemies.iterator(); it.hasNext(); ) {
				Agent ennemyClient = it.next();
				
				if (ennemyServer.getIdentifiant().equals(ennemyClient.getIdentifiant())) {
					it.remove();
				}
			}
		}
		
		//Il nous reste plus que les ennemis à supprimer coté client
		for (AgentEnnemy ennemyToRemove : copyEnnemies) {
			for (Iterator<AgentEnnemy> it = Map.getInstance().getAgentsEnnemy().iterator(); it.hasNext(); ) {
				AgentEnnemy ennemyClient = it.next();
				
				if (ennemyToRemove.getIdentifiant().equals(ennemyClient.getIdentifiant())) {
					it.remove();
					Map.getInstance().getAgents().remove(ennemyClient);
					Map.getInstance().getStart_Agents().remove(ennemyClient);
				}
			}
		}
		
		/**
		 * Mise a jour de l'etat des murs cassables
		 *
		 */
		boolean[][] murcassableServeur = player.getWallsBrokable();
		LOG.debug("Mur cassable : " + murcassableServeur);
		
		for(int x=0; x<murcassableServeur.length; x++) {
			for(int y=0; y<murcassableServeur[x].length; y++) {
				try {
					Map.getInstance().getStart_brokable_walls()[x][y] = murcassableServeur[x][y];	
				} catch(Exception e) {
					LOG.error(e.getMessage(), e);
				}
			}
		}
		
		/**
		 * Mise à jour des points du bomberman après explosion de bombe
		 */
		Agent bomberman = player.getBomberman();
		LOG.debug("Bomberman : " + bomberman);
		
		if(bomberman == null) return true;
		
		CapacityItem pointServer = bomberman.getCapacityItem(ItemType.POINT);
		LOG.debug("Point serveur : " + pointServer);
		
		if(pointServer == null) return true;
		
		CapacityItem pointClient = Map.getInstance().getCurrentAgentKind().getCapacityItem(ItemType.POINT);
		LOG.debug("Point client : " + pointClient);
		
		if(pointClient == null) return true;
		
		pointClient.setCapacityActualItem(pointServer.getCapacityActualItem());
		

		/**
		 * Ajout des bonus
		 */
		List<InfoItem> items = player.getItems();
		LOG.debug("Items : " + items);
		
		if(items == null) return true;
		
		for(InfoItem item : items) {
			Map.getInstance().getStart_Items().add(item);
		}
		
		return true;
	}

	@Override
	public Player receive(Object arg) {
		return null;
	}
}
