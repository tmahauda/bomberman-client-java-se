package universite.angers.master.info.client.bomberman.client.command.notify;

import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.client.ClientBombermanGame;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.client.bomberman.model.bomberman.map.Map;
import universite.angers.master.info.network.service.Commandable;

/**
 * Classe qui permet de vérifier si le jeu continue de tourner sur le serveur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientNotifyGame implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ClientNotifyGame.class);
	
	@Override
	public boolean send(Player player) {
		LOG.debug("Notify game player : " + player);
		
		boolean gameContinue = player.isGameContinue();
		LOG.debug("Game continue : " + gameContinue);
		
		ClientBombermanGame.getInstance().getControllerBombermanGame().getGame().setGameContinue(gameContinue);
		
		boolean gameOver = player.isGameOver();
		LOG.debug("Game over : " + gameOver);
		
		ClientBombermanGame.getInstance().getControllerBombermanGame().getGame().setGameOver(gameOver);
		
		//On change de map si la partie est remporté
		if(!gameContinue) {
			String filename = player.getPathMap();
			LOG.debug("File name map : " + filename);
			
			Map.getInstance().setFilename(filename);
		}
		
		return true;
	}

	@Override
	public Player receive(Object arg) {
		return null;
	}
}
