package universite.angers.master.info.client.bomberman.client.command.request;

import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.client.ClientBombermanGame;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.network.service.Commandable;

/**
 * Classe qui permet d'envoyer une requete au serveur pour s'identifier
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientRequestLogin implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ClientRequestLogin.class);

	private Player player;

	public ClientRequestLogin(Player player, String login, String password) {
		LOG.debug("Request login player : " + player);
		LOG.debug("Request login pseudo : " + login);
		LOG.debug("Request login password : " + password);

		String port = "" + ClientBombermanGame.getInstance().getClient().getClientSocket().getLocalPort();
		LOG.debug("Port socket : " + port);

		String ip = ClientBombermanGame.getInstance().getClient().getClientSocket().getLocalAddress().toString();
		LOG.debug("IP socket : " + ip);
		
		String id = port+ip;

		this.player = player;
		
		this.player.setPort(port);
		this.player.setIp(ip);
		this.player.setId(id);
		this.player.setLogin(login);
		this.player.setPassword(password);
		this.player.setRecord(false);
		this.player.getMessages().clear();
		
	}

	@Override
	public boolean send(Player player) {
		LOG.debug("Request login player : " + player);

		this.player.setRecord(player.isRecord());
		this.player.setMessages(player.getMessages());
		this.player.setPathMap(player.getPathMap());
		
		return true;
	}

	@Override
	public Player receive(Object arg) {
		this.player.setCommand("REQUEST_LOGIN");
		return this.player;
	}
}
