package universite.angers.master.info.client.bomberman.controller.actionnable;

import universite.angers.master.info.client.bomberman.model.bomberman.agent.Agent;

/**
 * Réaliser une action dans le jeu par un agent
 *
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Actionnable {
	
	/**
	 * Vérifier si une action est réalisable par un agent
	 * @param agent
	 * @param action
	 * @return vrai si on peut. Faux dans le cas contraire
	 */
	public boolean isLegalAction(Agent agent, AgentAction action);
	
	/**
	 * Effectuer une action par l'agent
	 * @param agent
	 * @param action
	 */
	public void doAction(Agent agent, AgentAction action);
}
