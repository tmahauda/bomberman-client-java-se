package universite.angers.master.info.client.bomberman.model.bomberman.agent.factory;

/**
 * Les différents type d'agent qui retourne une factory ennemy ou gentil
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentFactoryProvider {
	
	public static final char TYPE_AGENT_RAJION = 'R';
	public static final char TYPE_AGENT_BIRD = 'V';
	public static final char TYPE_AGENT_BASIQUE = 'E';
	public static final char TYPE_AGENT_BOMBERMAN = 'B';
	
	private AgentFactoryProvider() {
		
	}
	
	public static AgentFactory getAgentFactory(char typeAgent) {
		if(isGentil(typeAgent)){
			return AgentFactoryKind.getInstance();
		}
		else{
			return AgentFactoryEnnemy.getInstance();
		}
	}
	
	private static boolean isGentil(char typeAgent) {
		return typeAgent == TYPE_AGENT_BOMBERMAN;
	}
}