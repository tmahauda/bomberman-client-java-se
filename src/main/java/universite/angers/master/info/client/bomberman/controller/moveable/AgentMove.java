package universite.angers.master.info.client.bomberman.controller.moveable;

/**
 * Enumération des déplacements autorisés sur le plateau
 * - Haut
 * - Bas
 * - Gauche
 * - Droite
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public enum AgentMove {
	MOVE_UP, MOVE_DOWN, MOVE_LEFT, MOVE_RIGHT, STOP;
}
