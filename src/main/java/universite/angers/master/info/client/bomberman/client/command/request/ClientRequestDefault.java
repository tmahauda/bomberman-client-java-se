package universite.angers.master.info.client.bomberman.client.command.request;

import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.network.service.Commandable;

/**
 * Classe qui permet d'envoyer une requete par défaut au serveur via "receive()"
 * et de recevoir une reponse dans le "send()"
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientRequestDefault implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ClientRequestDefault.class);

	@Override
	public boolean send(Player player) {
		LOG.debug("Request default player : " + player);
		
		for(String message : player.getMessages()) {
			LOG.debug(message);
		}
		
		return true;
	}

	@Override
	public Player receive(Object arg) {
		return null;
	}
}
