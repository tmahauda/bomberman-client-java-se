# Bomberman Client

<div align="center">
<img width="250" height="250" src="client.png">
</div>

## Description du projet

Application bureautique réalisée avec Java SE en MASTER INFO 1 à l'université d'Angers dans le cadre du module "Programmation réseaux" durant l'année 2019-2020. \
Ce projet permet d'émettre des requêtes au serveur du jeu Bomberman.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par :
- Théo MAHAUDA : tmahauda@etud.univ-angers.fr ;
- Mohamed OUHIRRA : mouhirra@etud.univ-angers.fr.

### Encadrants

Ce projet fut encadré par un enseignant de l'unversité d'Angers :
- Benoît DA MOTA : benoit.da-mota@univ-angers.fr.

## Organisation

Ce projet a été agit au sein de l'université d'Angers dans le cadre du module "Programmation réseaux" du MASTER INFO 1.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2020 sur la période du confinement COVID-19 à la maison au mois de Mars-Avril. \
Il a été terminé et rendu le 09/04/2020.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java ;
- Eclipse ;
- Maven ;
- Sockets TCP ;
- Thread.

## Objectif

Permettre à l'utilisateur de jouer au jeu Bomberman en se connectant à un serveur qui gère les parties.

## Architecture

<img src="src/main/resources/images/Architecture.png"/>

### Requetes / Notifications

Il y a deux types de communication entre le serveur et le client, soit on communique un traitement comme par exemple faire bouger l'agent Bomberman, et dans ce cas on utilise des requêtes qui envoit une demande au serveur afin de recevoir un traitement particulier, soit on communique des informations par exemple quand un joueur gagne la partie, et dans ce cas on utilise les notifications qui envoient une demande au serveur sans pour autant recevoir une réponse.
Afin de créer une requête ou bien une notification, il faut créer une classe qui implémente l'interface Commandable du coté du client et du serveur, puis l'enregistrer au niveau du "ServiceClient" et "ServiceServer".

### Communication avec le serveur

À travers des sockets, le client et le serveur communiquent en JSON, les données sont converties en JSON à l'envoi et reconvertis en Objet à la réception en utilisant une classe Player qui contient tous les attributs dont on a besoin pour transmettre non seulement les données du jeu mais aussi les messages à transmettre à chaque utilisateur.
Les requêtes envoyées par le client suivent un mécanisme de verrou, cela veut dire qu'un client ne peut envoyer qu'une requête à la fois afin de synchroniser les échanges avec le serveur.

## Fonctionnalités implémentées

### Connexion d'un joueur

La connexion d'un joueur commence d'abord du coté du client, à travers une requête "ClientRequestLogin", qui permet d'envoyer le login (Pseudo) et le mot de passe du client au serveur, ainsi qu'un identifiant unique au client crée à partir de l'association entre son adresse Ip et le numéro du port qu'il utilise.

<img src="src/main/resources/images/Login.png"/>

### Déconnexion d'un joueur

La déconnexion d'un joueur se fait à travers la classe "ClientRequestLogout", de la même manière que dans la connexion, le client envoie une demande de déconnexion au serveur afin que celui-ci le supprime de la map et puis envois une notification aux clients après le succès de la suppression du joueur.

### Chat entre joueurs

Dans un premier temps, l'idée d'implementer un chat vient du fait qu'on besoin d'une vue permettant d'afficher les différentes notifications et messages envoyés par le serveur et qui expriment l'état du jeu (A qui le tour de jouer, la connexion d'un nouveau joueur, etc....). Dans un second temps, le chat pourra aussi servir à la communication entre les joueurs comme un chat ordinaire qui est disponible sur la plupart des plateformes multijoueur.

### Les actions possibles du joueur

Procédure anti-triche : afin qu'un client puisse effectuer une action dans le jeu, il doit d'abord envoyer une requête au serveur, puis le serveur valide ou pas l'action de son coté et renvoit le nouvel État du jeu après avoir exécuté l'action du joueur.
  
#### Poser une bombe

La classe "ClientRequestActionPutBomb" s'occupe de l'envoi de la demande au serveur pour qu'un joueur puisse poser une bombe et recois par la suite le nouvel état des bombes.

<img src="src/main/resources/images/PoserBombe.png"/>

#### Déplacer le Bomberman

Pour bouger un agent Bomberman, on utilise la classe "ClientRequestMoveBomberman" qui de la même manière que les autres actions du jeu, permet d'envoyer une demande, le serveur répond par une requête après avoir validé l'action de son coté et renvoit le nouvel État du jeu.

<img src="src/main/resources/images/Mouvement.png"/>

### Mise à jour du jeu envoyé par le serveur

À chaque tour passé, les agents ennemis ainsi que les bombes changent d'état, et pour notifier le client à chaque fois de ce changement, la classe "ClientNotifyTurn" permet d'envoyer une notification au serveur afin que ce dernier met à jour les changements s'effectuant dans le jeu, ainsi que l'état du jeu à travers les notifications présentées ci-dessous.

#### L'état de la partie

À chaque tour l'état du jeu peut changer, un joueur peut gagner ou perdre donc après chaque tour le client envoit une notification "ClientNotifyGame" qui permet de récupérer l'état du jeu du serveur.

#### Le déplacement du Bomberman

Après chaque déplacement de l'agent Bomberman, le client envoi une notification "ClientNotifyMoveBomberman" afin de récupérer les coordonnées de l'agent, puis met à jour la map avec les coordonnées reçu du serveur.

#### Le déplacement des ennemis

Après chaque tour, les agents ennemis changent de position, le client envoi une notification "ClientNotifyMoveEnnemy" afin de récupérer les coordonnées des agents ennemis, puis les mettent à jour la map avec les nouveaux coordonnées reçus du serveur.

#### Les bombes posées

Quand un joueur pose une bombe, une notification "ClientNotifyActionPutBomb" est envoyée au serveur afin de récupérer le nouvel état des bombes.

#### Les bonus appliqués sur le Bomberman

Quand un joueur explose un mur, il a le droit à un bonus, et afin de récupérer cette information du serveur, le client envoit une notification "ClientNotifyApplyBonus", celle-ci permet de mettre à jour le bonus du joueur après l'application de ces derniers par le serveur.

#### Les impacts d'attaques des ennemis sur le Bomberman

Les agents ennemis peuvent attaquer l'agent Bomberman depandement de leur nature, donc pour mettre à jour le nouvel état du Bomberman après une attaque, le client envoie une notification "ClientNotifyAttackBomberman" et recois en retour la réponse du serveur qui contient le nouvel état du Bomberman.

### Fin de partie

#### Perdu

Quand un joueur se fait éliminer par un ennemi après un tour, et à travers la classe "ClientNotifyGame'", si le joueur a perdu, dans ce cas une fenêtre pop-up s'affiche pour annoncer au jour la fin de la partie comme dans la figure ci dessous.

<img src="src/main/resources/images/Perdu.png"/>

#### Gagné

La même chose se passe quand un joueur gagne la partie, à travers la class "ClientNotifyGame", le client reçoit le nouvel État du jeu, si la partie est fini et le joueur a gagné, le serveur lui propose de passer à un autre niveau ou de quitter, et si le joueur choisit de continuer, le serveur charge une nouvelle map en se basant sur le niveau que le joueur a atteint.

<img src="src/main/resources/images/Gagne.png"/>

## Fonctionnalités non implémentées

Les fonctionnalités qui restent à implementer sont les suivantes :
- Communication entre joueurs à travers le chat : Certes le chat que nous avons implémenté permet d'afficher aux joueurs les différentes notifications utiles au déroulement de la partie, il reste donc à implemnter un vrai chat qui permet aux joueurs de communiquer entre eux durant une partie.